import { createWidget } from "discourse/widgets/widget";

createWidget("close-try-modal-button", {
  tagName:
    "button.close-btn.coloredBG.text-white.py-2.px-4.my-4.mr-4.rounded-full.font-bold.text-sm",

  html() {
    return "Close";
  },
  click(event) {
    event.stopPropagation();
    const modalDialog = document.querySelector("div[data-popup-wrapper]");
    if (modalDialog) {
      modalDialog.classList.add("hidden");
    }
  },
});

import { createWidget } from "discourse/widgets/widget";

createWidget("try-button", {
  tagName:
    "button.coloredBG.inline-block.font-bold.my-2.mr-2.text-white.px-4.py-2.rounded-full.self-center",

  html() {
    return "TRY";
  },
  click(event) {
    event.stopPropagation();
    const modalDialog = document.querySelector("div[data-popup-wrapper]");
    if (modalDialog) {
      modalDialog.classList.remove("hidden");
    }
  },
});

import { createWidget } from "discourse/widgets/widget";
import hbs from "discourse/widgets/hbs-compiler";

createWidget("header-contents", {
  tagName: "div.contents.clearfix",
  template: hbs`
    {{home-logo attrs=attrs}}
    <nav class="bg-white text-base rightBorder items-center self-stretch my-4 relative inset-0 flex flex-row justify-end flex-1">
      <ul class="primeroNavMenu flex-row flex items-center">
        <li class="my-2 mx-2">
          <a href="https://support.primero.org">HOME</a>
        </li>
        <li class="my-2 mx-2">
          <a href="https://support.primero.org/releases">RELEASES</a>
        </li>
        <li class="my-2 mx-2">
          <a href="https://support.primero.org/documentation">DOCUMENTATION</a>
        </li>
        <li class="my-2 mx-2">
          <a href="https://support.primero.org/support">SUPPORT</a>
        </li>
        <li class="my-2 mx-2">
          <a href="https://community.primero.org/">COMMUNITY</a>
        </li>
      </ul>
      {{try-button attrs=attrs}}
      {{try-modal attrs=attrs}}
      </nav>
      {{#if attrs.topic}}
      {{header-topic-info attrs=attrs}}
      {{/if}}
      <div class="panel clearfix">{{yield}}</div>
      `,
});

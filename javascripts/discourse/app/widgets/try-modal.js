import { createWidget } from "discourse/widgets/widget";
import hbs from "discourse/widgets/hbs-compiler";

createWidget("try-modal", {
  tagName: "div.contents.clearfix",
  template: hbs`
    <div data-popup-wrapper="" class="hidden z-10 bg-opacity-50 bg-black justify-center items-center fixed h-full inset-0 p-4 flex overflow-y-auto">
      <div class="popup bg-white w-full border-4 p-4 m-2 flex flex-col flex-shrink overflow-y-auto" data-popup-content="">
        <div class="text-center font-bold">
          <a class="coloredBG btn-link block text-white visited:text-white py-2 px-4 rounded-full mb-4 no-underline text-sm" target="_blank" rel="noopener noreferrer" href="https://playground.primerodev.org/">
            Click here to try Primero v2!
          </a>
          <table class="promotion-table border-collapse mb-4 w-full text-left">
            <thead>
              <tr>
                <th class="font-bold text-black">Role</th>
                <th class="font-bold text-black">Login</th>
                <th class="font-bold text-black">Password</th>
              </tr>
            </thead>
            <tbody class="border-0 text-black">
              <tr>
                <td>Child Protection Worker</td>
                <td>primero_cp</td>
                <td>primero2020</td>
              </tr>
              <tr>
                <td>Child Protection Manager</td>
                <td>primero_mgr_cp</td>
                <td>primero2020</td>
              </tr>
            </tbody>
          </table>
        </div>
        {{close-try-modal-button attrs=attrs}}
      </div>
    </div>
  `,
});
